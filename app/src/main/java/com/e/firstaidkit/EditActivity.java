package com.e.firstaidkit;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.e.firstaidkit.MainActivity.CURRENT_POSITION;
import static com.e.firstaidkit.MainActivity.FORMATTER;
import static com.e.firstaidkit.MainActivity.SHARED_PREF;
import static com.e.firstaidkit.MainActivity.currentPosition;
import static com.e.firstaidkit.MainActivity.listOfItemNames_old;

public class EditActivity extends AppCompatActivity {
    private final Calendar dateAndTime = Calendar.getInstance();
    private List<Medicine> states = new ArrayList();
    private Integer add = 0, edit;
    private Boolean star = false;
    //private List<Medicine> med;
    private Button dateButton;
    // установка обработчика выбора даты
    private final DatePickerDialog.OnDateSetListener dateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
        dateAndTime.set(Calendar.YEAR, year);
        dateAndTime.set(Calendar.MONTH, monthOfYear);
        dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        setInitialDateTime();
    };
    private TextView nameTextView, doseTextView, formTextView, numberTextView, commentTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        nameTextView = findViewById(R.id.name);
        doseTextView = findViewById(R.id.dose);
        formTextView = findViewById(R.id.form);
        dateButton = findViewById(R.id.date);
        numberTextView = findViewById(R.id.number);
        commentTextView = findViewById(R.id.comment);

        Button saveButton = findViewById(R.id.saveBut);
        Button deleteButton = findViewById(R.id.deleteBut);
        Button plusButton = findViewById(R.id.buttonPlus);
        Button minusButton = findViewById(R.id.buttonMin);

        Intent mIntent = getIntent();
        edit = mIntent.getIntExtra("EDIT", -1);

//        Toast toast = Toast.makeText(getApplicationContext(), edit.toString(), Toast.LENGTH_SHORT);
//        toast.show();

        if (edit != -1) {
            loadData(listOfItemNames_old[currentPosition]);
            Medicine state = states.get(edit);
            nameTextView.setText(state.getName());
            doseTextView.setText(state.getDose());
            formTextView.setText(state.getForm());
            numberTextView.setText(state.getCurrentNumber().toString());
            dateButton.setText(FORMATTER.format(state.getEndDate()));
            Date now = Calendar.getInstance().getTime();
            if (now.after(state.getEndDate())) {
                dateButton.setTextColor(getResources().getColor(R.color.red));
            } else {
                dateButton.setTextColor(getResources().getColor(R.color.mygreen));
            }
            commentTextView.setText(state.getComment());
            star = state.getStar();
        }

        dateButtonListener();
        plusButtonListener(plusButton);
        minusButtonListener(minusButton);
        deleteButtonListener(deleteButton);
        saveButtonListener(saveButton);
    }

    private void dateButtonListener() {
        dateButton.setOnClickListener(v -> new DatePickerDialog(EditActivity.this, dateSetListener,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show());
    }

    private void plusButtonListener(Button plusButton) {
        plusButton.setOnClickListener(v -> {
            int num = 0;
            if (numberTextView.getText().length() != 0) {
                num = Integer.parseInt(numberTextView.getText().toString());
            }
            num++;
            numberTextView.setText(Integer.toString(num));
        });
    }

    private void minusButtonListener(Button minusButton) {
        minusButton.setOnClickListener(v -> {
            Integer num = 0;
            if (numberTextView.getText().length() != 0) {
                num = Integer.parseInt(numberTextView.getText().toString());
            }
            num--;
            if (num < 0) {
                num = 0;
            }
            numberTextView.setText(num.toString());
        });
    }

    private void deleteButtonListener(Button deleteButton) {
        deleteButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle(R.string.warning);
            builder.setMessage("Удалить текущую запись?");
            builder.setPositiveButton("Да", (dialog, which) -> {
                if (edit != -1) {
                    int index = edit;
                    states.remove(index);
                    saveData(listOfItemNames_old[currentPosition], currentPosition);
                }
                Toast toast = Toast.makeText(getApplicationContext(), "Данные удалены", Toast.LENGTH_SHORT);
                toast.show();
                Intent intent = new Intent(EditActivity.this, MainActivity.class);
                startActivity(intent);
            });
            builder.setNegativeButton("Нет", (dialog, which) -> {
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }

    private void saveButtonListener(Button saveButton) {
        saveButton.setOnClickListener(v -> {
            if (nameTextView.getText().toString().equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
                builder.setCancelable(true);
                builder.setTitle(R.string.warning);
                builder.setMessage("Пожалуйста, введите название препарата");
                builder.setPositiveButton("Ок", (dialog, which) -> {
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                if (edit != -1) {
                    int index = edit;
                    add = edit;
                    states.remove(index);
                } else {
                    loadData(listOfItemNames_old[currentPosition]);
                }
                states.add(add, new Medicine(nameTextView.getText().toString(), doseTextView.getText().toString(),
                        formTextView.getText().toString(), Integer.valueOf(numberTextView.getText().toString()),
                        getParseDate(), commentTextView.getText().toString(), star));
                saveData(listOfItemNames_old[currentPosition], currentPosition);
                Toast toast = Toast.makeText(getApplicationContext(), "Данные сохранены", Toast.LENGTH_SHORT);
                toast.show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private Date getParseDate() {
        try {
            return FORMATTER.parse(dateButton.getText().toString());
        } catch (ParseException e) {
            return Calendar.getInstance().getTime();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        boolean nameFlag, doseFlag, formFlag, dateFlag, commentFlag, numberFLag;
        if (edit != -1) {
            loadData(listOfItemNames_old[currentPosition]);
            Medicine state = states.get(edit);
            nameFlag = nameTextView.getText().toString().equals(state.getName());
            doseFlag = doseTextView.getText().toString().equals(state.getDose());
            formFlag = formTextView.getText().toString().equals(state.getForm());
            dateFlag = dateAndTime.getTime().equals(state.getEndDate());
            numberFLag = numberTextView.getText().toString().equals(state.getCurrentNumber().toString());
            commentFlag = commentTextView.getText().toString().equals(state.getComment());
        } else {
            nameFlag = nameTextView.getText().toString().equals("");
            doseFlag = doseTextView.getText().toString().equals("");
            formFlag = formTextView.getText().toString().equals("");
            dateFlag = dateButton.getText().toString().equals("");
            numberFLag = numberTextView.getText().toString().equals("");
            commentFlag = commentTextView.getText().toString().equals("");
        }

        if (nameFlag && doseFlag && formFlag && dateFlag && numberFLag && commentFlag) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
            builder.setCancelable(true);
            builder.setTitle("Внимание");
            builder.setMessage("Покинуть страницу без сохранения?");
            builder.setPositiveButton("Уйти", (dialog, which) -> {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            });
            builder.setNegativeButton("Остаться", (dialog, which) -> {
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void setInitialDateTime() {
        dateButton.setText(FORMATTER.format(dateAndTime.getTime()));
        Date now = Calendar.getInstance().getTime();
        if (now.after(dateAndTime.getTime())) {
            dateButton.setTextColor(getResources().getColor(R.color.red));
        } else {
            dateButton.setTextColor(getResources().getColor(R.color.mygreen));
        }
    }

    private void saveData(String item, int currentPosition) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(states);
        editor.putString(item, json);
        editor.apply();
        sharedPreferences = getSharedPreferences(CURRENT_POSITION, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putInt(CURRENT_POSITION, currentPosition);
        editor.apply();
    }

    private void loadData(String item) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(item, null);
        Type type = new TypeToken<ArrayList<Medicine>>() {
        }.getType();
        try {
            states = gson.fromJson(json, type);
        } catch (JsonSyntaxException ex) {
            states = new ArrayList<>();
        }
        Collections.sort(states, (o1, o2) -> o1.getName().compareTo(o2.getName()));
    }

}