package com.e.firstaidkit;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.e.firstaidkit.MainActivity.FORMATTER;
import static com.e.firstaidkit.MainActivity.stars;

public class MedAdapter extends ArrayAdapter<Medicine> {

    private final LayoutInflater inflater;
    private final int layout;
    private final List<Medicine> states;

    public MedAdapter(Context context, int resource, List<Medicine> states) {
        super(context, resource, states);
        this.states = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    @SuppressLint("SetTextI18n")
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(this.layout, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Medicine state = states.get(position);

        viewHolder.name.setText(state.getName() + " " + state.getDose());
        viewHolder.number.setTextColor(convertView.getResources().getColor(R.color.black));
        viewHolder.date.setTextColor(convertView.getResources().getColor(R.color.black));
        viewHolder.star.setImageResource(android.R.drawable.btn_star_big_off);

        if (state.getForm() == null || state.getForm().isEmpty()) {
            viewHolder.form.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.form.setVisibility(View.VISIBLE);
            viewHolder.form.setText(state.getForm());
        }

        viewHolder.number.setVisibility(View.VISIBLE);
        viewHolder.number.setText(state.getCurrentNumber().toString() + " " + getContext().getString(R.string.med_adapter_number));

        if (state.getEndDate() == null) {
            viewHolder.date.setText("?");
            viewHolder.bar.setBackgroundColor(convertView.getResources().getColor(R.color.yellow));
        } else {
            viewHolder.clock.setVisibility(View.VISIBLE);
            viewHolder.date.setVisibility(View.VISIBLE);
            viewHolder.date.setText(FORMATTER.format(state.getEndDate()));
            Date now = Calendar.getInstance().getTime();
            viewHolder.bar.setBackgroundColor(convertView.getResources().getColor(R.color.mygreen));

            if (now.after(state.getEndDate())) {
                viewHolder.bar.setBackgroundColor(convertView.getResources().getColor(R.color.red));
                viewHolder.date.setTextColor(convertView.getResources().getColor(R.color.red));
            }
            //Toast toast = Toast.makeText(getContext(), curdate, Toast.LENGTH_SHORT);
            //toast.show();
            if (state.getCurrentNumber() == 0) {
                viewHolder.bar.setBackgroundColor(convertView.getResources().getColor(R.color.red));
                viewHolder.number.setTextColor(convertView.getResources().getColor(R.color.red));
            }

        }

        if (state.getComment() == null || state.getComment().isEmpty()) {
            viewHolder.comment.setVisibility(View.GONE);
        } else {
            viewHolder.comment.setVisibility(View.VISIBLE);
            viewHolder.comment.setText(state.getComment());
        }

        if (state.getStar()) {
            viewHolder.star.setImageResource(android.R.drawable.btn_star_big_on);
        }

        viewHolder.lay.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), EditActivity.class);
            intent.putExtra("EDIT", position);
            getContext().startActivity(intent);
        });

        viewHolder.lay.setOnLongClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(true);
            builder.setTitle(R.string.warning);
            builder.setMessage("Удалить \"" + state.getName() + "\"?");
            builder.setPositiveButton("Да", (dialog, which) -> {
                states.remove(state);
                notifyDataSetChanged();
                Toast toast = Toast.makeText(getContext(), "Запись удалена", Toast.LENGTH_SHORT);
                toast.show();
            });
            builder.setNegativeButton("Нет", (dialog, which) -> {
            });
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        });

        viewHolder.star.setOnClickListener(v -> {
            if (!state.getStar()) {
                notifyDataSetChanged();
                viewHolder.star.setImageResource(android.R.drawable.btn_star_big_on);
                state.setStar(true);
            } else {
                if (stars) {
                    states.remove(state);
                    notifyDataSetChanged();
                }

                viewHolder.star.setImageResource(android.R.drawable.btn_star_big_off);
                state.setStar(false);
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        final TextView name, dose, form, number, date, comment, bar;
        final ImageView clock, star;
        final LinearLayout lay;

        ViewHolder(View view) {
            name = view.findViewById(R.id.name);
            dose = view.findViewById(R.id.dose);
            form = view.findViewById(R.id.form);
            number = view.findViewById(R.id.number);
            date = view.findViewById(R.id.date);
            clock = view.findViewById(R.id.clockView);
            comment = view.findViewById(R.id.comment);
            star = view.findViewById(R.id.starimageView);
            bar = view.findViewById(R.id.textViewBar);
            lay = view.findViewById(R.id.linearlayout);

        }
    }
}