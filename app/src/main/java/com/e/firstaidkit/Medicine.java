package com.e.firstaidkit;

import java.util.Date;

public class Medicine {

    private String name;
    private String dose;
    private String form;
    private Integer currentNumber;
    private Date endDate;
    private String comment;
    private Boolean star;

    public Medicine(String name, String dose, String form, Integer currentNumber, Date endDate, String comment, Boolean star) {
        this.name = name;
        this.dose = dose;
        this.form = form;
        this.currentNumber = currentNumber;
        this.endDate = endDate;
        this.comment = comment;
        this.star = star;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDose() {
        return this.dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getForm() {
        return this.form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public Integer getCurrentNumber() {
        if (currentNumber == null) {
            currentNumber = 0;
        }
        return this.currentNumber;
    }

    public void setCurrentNumber(Integer currentNumber) {
        this.currentNumber = currentNumber;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getStar() {
        return this.star;
    }

    public void setStar(Boolean star) {
        this.star = star;
    }

}
