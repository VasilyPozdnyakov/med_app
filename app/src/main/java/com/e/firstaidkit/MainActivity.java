package com.e.firstaidkit;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class MainActivity extends AppCompatActivity {

    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd MMMM, yyyy");
    public static final String SHARED_PREF = "sharedpref";
    public static final String CURRENT_POSITION = "position";
    public static final String NEW_NAME = "name";
    private static final int SHAW_ALL_ITEM_INDEX = 4;
    private static final int NO_GOOD_ITEM_INDEX = 3;
    private static final int NO_EXPIRED_ITEM_INDEX = 2;
    private static final int STARS_ITEM_INDEX = 1;
    public static String[] listOfItemNames;
    public static String[] listOfItemNames_old = {"List 1", "List 2", "List 3"};
    public static Map<Integer, String> map = new HashMap<>();
    public static int currentPosition;
    public static boolean noGood = false, noExpired = false, stars = false, shawAll = true;
    private final List<Medicine> states_copy = new ArrayList();
    private List<Medicine> states;
    private ListView medList;
    private String item;
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        MenuItem menuItem = menu.findItem(R.id.action_search);
//        menuItem.setVisible(false);
//        return true;
//    }


    private void spinnerListener(Spinner spinner) {
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //item = (String) parent.getItemAtPosition(position);
                item = (String) map.get(position);
                saveData(listOfItemNames_old[currentPosition], currentPosition);
                currentPosition = position;
                loadData(listOfItemNames_old[currentPosition]);
                for (int i = 0; i < states.size(); i++) {
                    try {
                        states.get(i).getStar();
                    } catch (Exception e) {
                        states.get(i).setStar(false);
                    }
                }

                saveData(listOfItemNames_old[currentPosition], currentPosition);
                MedAdapter stateAdapter = new MedAdapter(MainActivity.this, R.layout.list_item, states);
                medList.setAdapter(stateAdapter);

                noGood = false;
                noExpired = false;
                stars = false;
                shawAll = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        spinner.setOnItemSelectedListener(itemSelectedListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = findViewById(R.id.spinner);
        medList = findViewById(R.id.medList);
        Button butAdd = findViewById(R.id.butAdd);
        ImageView settings = findViewById(R.id.setView);

        currentPosition = loadCurrentPosition();
        map.put(0, "List 1");
        map.put(1, "List 2");
        map.put(2, "List 3");

        listOfItemNames = loadNewName();
        loadData(listOfItemNames_old[currentPosition]);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listOfItemNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(currentPosition);

        spinnerListener(spinner);
        settingListener(settings, adapter);
        butAddListener(butAdd);
    }

    private void settingListener(ImageView settings, ArrayAdapter<String> adapter) {
        settings.setOnClickListener(v -> {
            PopupMenu popup = new PopupMenu(MainActivity.this, settings);
            popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

            popup.setOnMenuItemClickListener(item -> {
                if (item.getTitle().equals(getResources().getString(R.string.rename))) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = this.getLayoutInflater();
                    final View layoutView = inflater.inflate(R.layout.dialog_edit, null);

                    builder.setView(layoutView);
                    EditText text1 = layoutView.findViewById(R.id.name);
                    text1.setText(listOfItemNames[currentPosition]);
                    builder.setCancelable(true);
                    builder.setTitle("Изменение названия");
                    builder.setNeutralButton("Сохранить", (dialog, which) -> {
                        listOfItemNames[currentPosition] = text1.getText().toString();
                        adapter.notifyDataSetChanged();
                        saveNewName(listOfItemNames);
                        Toast toast = Toast.makeText(getApplicationContext(), "Данные сохранены", Toast.LENGTH_SHORT);
                        toast.show();
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

                if (item.getTitle().equals(getResources().getString(R.string.rating))) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.avappinc.sounds")));
                }

                if (item.getTitle().equals(getResources().getString(R.string.noexpired))) {
                    if (!noExpired) {
                        item.setChecked(true);
                        noExpired = true;
                        noGood = false;
                        stars = false;
                        shawAll = false;
                        popup.getMenu().getItem(NO_GOOD_ITEM_INDEX).setChecked(noGood);
                        popup.getMenu().getItem(STARS_ITEM_INDEX).setChecked(stars);
                        popup.getMenu().getItem(SHAW_ALL_ITEM_INDEX).setChecked(shawAll);
                        Date now = Calendar.getInstance().getTime();
                        refreshStates(it -> now.after(it.getEndDate()));
                    } else {
                        item.setChecked(false);
                        noExpired = false;
                        shawAllStates();
                    }
                }

                if (item.getTitle().equals(getResources().getString(R.string.nogood))) {
                    if (!noGood) {
                        item.setChecked(true);
                        noGood = true;
                        noExpired = false;
                        stars = false;
                        shawAll = false;
                        popup.getMenu().getItem(NO_EXPIRED_ITEM_INDEX).setChecked(noExpired);
                        popup.getMenu().getItem(STARS_ITEM_INDEX).setChecked(stars);
                        popup.getMenu().getItem(SHAW_ALL_ITEM_INDEX).setChecked(shawAll);
                        Date now = Calendar.getInstance().getTime();
                        refreshStates(it -> !now.after(it.getEndDate()));
                    } else {
                        item.setChecked(false);
                        noGood = false;
                        shawAllStates();
                    }
                }

                if (item.getTitle().equals(getResources().getString(R.string.star))) {
                    if (!stars) {
                        item.setChecked(true);
                        stars = true;
                        noGood = false;
                        noExpired = false;
                        shawAll = false;
                        popup.getMenu().getItem(NO_EXPIRED_ITEM_INDEX).setChecked(noExpired);
                        popup.getMenu().getItem(NO_GOOD_ITEM_INDEX).setChecked(noGood);
                        popup.getMenu().getItem(SHAW_ALL_ITEM_INDEX).setChecked(shawAll);
                        refreshStates(it -> !it.getStar());
                    } else {
                        item.setChecked(false);
                        stars = false;
                        shawAllStates();
                    }
                }

                if (item.getTitle().equals(getResources().getString(R.string.showAll))) {
                    if (!shawAll) {
                        item.setChecked(true);
                        stars = false;
                        noGood = false;
                        noExpired = false;
                        shawAll = true;
                        popup.getMenu().getItem(STARS_ITEM_INDEX).setChecked(stars);
                        popup.getMenu().getItem(NO_EXPIRED_ITEM_INDEX).setChecked(noExpired);
                        popup.getMenu().getItem(NO_GOOD_ITEM_INDEX).setChecked(noGood);
                    } else {
                        item.setChecked(false);
                        shawAll = false;
                    }
                    shawAllStates();
                }

                if (item.getTitle().equals(getResources().getString(R.string.search))) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = this.getLayoutInflater();
                    final View layoutView = inflater.inflate(R.layout.dialog_edit, null);

                    builder.setView(layoutView);
                    EditText editText = layoutView.findViewById(R.id.name);
                    editText.setText("");
                    builder.setCancelable(true);
                    builder.setTitle("Введите название");
                    builder.setPositiveButton(getResources().getString(R.string.search), (dialog, which) -> {
                        shawAll = false;
                        refreshStates(it -> !searchName(editText, it));
                    });
                    builder.setNegativeButton("Отмена", (dialog, which) -> dialog.cancel());

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

                return true;
            });

            popup.getMenu().getItem(SHAW_ALL_ITEM_INDEX).setChecked(shawAll);
            popup.getMenu().getItem(NO_GOOD_ITEM_INDEX).setChecked(noGood);
            popup.getMenu().getItem(NO_EXPIRED_ITEM_INDEX).setChecked(noExpired);
            popup.getMenu().getItem(STARS_ITEM_INDEX).setChecked(stars);

            popup.show();
        });
    }

    private void shawAllStates() {
        shawAll = true;
        states_copy.clear();
        states_copy.addAll(states);
        MedAdapter stateAdapter = new MedAdapter(MainActivity.this, R.layout.list_item, states_copy);
        medList.setAdapter(stateAdapter);
    }

    private void refreshStates(Predicate<Medicine> filter) {
        states_copy.clear();
        states_copy.addAll(states);
        states_copy.removeIf(filter);
        MedAdapter stateAdapter = new MedAdapter(MainActivity.this, R.layout.list_item, states_copy);
        medList.setAdapter(stateAdapter);
    }

    private boolean searchName(EditText editText, Medicine it) {
        return it.getName().toLowerCase().contains(editText.getText().toString().toLowerCase());
    }

    private void butAddListener(Button butAdd) {
        butAdd.setOnClickListener(v -> {
            currentPosition = loadCurrentPosition();
            saveData(listOfItemNames_old[currentPosition], currentPosition);
            Intent intent = new Intent(getApplicationContext(), EditActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        currentPosition = loadCurrentPosition();
        saveData(listOfItemNames_old[currentPosition], currentPosition);
    }

    private void loadData(String test) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(test, null);
        Type type = new TypeToken<ArrayList<Medicine>>() {
        }.getType();
        try {
            states = gson.fromJson(json, type);
        } catch (JsonSyntaxException ex) {
            states = new ArrayList<>();
        }
        Collections.sort(states, (o1, o2) -> o1.getName().compareTo(o2.getName()));
    }

    private int loadCurrentPosition() {
        SharedPreferences sharedPreferences = getSharedPreferences(CURRENT_POSITION, MODE_PRIVATE);
        return sharedPreferences.getInt(CURRENT_POSITION, 0);
    }

    private void saveData(String item, int currentPosition) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(states);
        editor.putString(item, json);
        editor.apply();

        sharedPreferences = getSharedPreferences(CURRENT_POSITION, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putInt(CURRENT_POSITION, currentPosition);
        editor.apply();
    }

    private void saveNewName(String[] listofnames) {
        SharedPreferences sharedPreferences = getSharedPreferences(NEW_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(listofnames);
        editor1.putString(NEW_NAME, json);
        editor1.apply();
    }

    private String[] loadNewName() {
        SharedPreferences sharedPreferences = getSharedPreferences(NEW_NAME, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(NEW_NAME, null);
        Type type = new TypeToken<String[]>() {
        }.getType();
        String[] temp;
        if (gson.fromJson(json, type) != null) {
            temp = gson.fromJson(json, type);
        } else {
            temp = new String[]{"Аптечка #1"};
        }
        return temp;
    }

    // слушатель выбора в списке
//        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//
//                // получаем выбранный пункт
//                Medicine selectedState = (Medicine) parent.getItemAtPosition(position);
//                Toast.makeText(getApplicationContext(), "Был выбран пункт " + selectedState.getName(),
//                        Toast.LENGTH_SHORT).show();
//            }
//        };
//        countriesList.setOnItemClickListener(itemListener);


}

